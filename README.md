# 目录中的项目都是独立的，直接打开子目录

#### anyline-service-sync
异构数据库同步任务  

```
 数据库配置说明
 BS_DATASOURCE:配置数据源  注意帐号密码URL等敏感信息最好经过非对称加密,把私钥放在jar中再把jar加密
      CODE                    : 数据源标识,后续将根据这个值切换数据源
      DRIVER                  : 驱动类(根据数据库类型,如:com.mysql.cj.jdbc.Driver)
      URL                     : JDBC连接
      ACCOUNT                 : 帐号
      PASSWORD                : 密码
      TYPE_CLASS              : 连接池类(默认:com.zaxxer.hikari.HikariDataSource)
 SYNC_TASK:同步配置
      TRIGGER_TYPE_CODE       : 触发方式 0:定时 1:被动
      DATA_STATUS             : 状态 0:不可用 不会执行 1:可用
      TYPE_CODE               : 0:全量(先truncate目标表再insert)
                                1:增量
                                2:更新
      INTERVALS               : 执行间隔时间(秒)
      SCHEDULE_ID             : 定时器(如果任务本身就在定时器中不要设置这个值)
      SRC_DATASOURCE_CODE     : 源数据源(BS_DATASOURCE.CODE)
      SRC_TABLE               : 源表
                                如果有多个表以,分隔,同时保持与TAR_TABLE中的表数量一致
                                如果要同步整个数据库可以SRC_TABLE设置成(TAR_TABLE值将被忽略)
                                -T1,T2 表示同步除了T1,T2的所有表

      TAR_DATASOURCE_CODE     : 目标数据源
      TAR_TABLE               : 目标表(SRC_TABLE中出现时 忽略当前值)
      COLS                    : 源表与目标表的 列对应关系(源列名:目标列名)
                                如ID:CD,NAME:NM,如果有一样列可以ID,NAME:NM,表示全部
                                如果SRC_TABLE中设置了多个值或,忽略当前值
      ORDER_COLUMN            : 排序列
      PK                      : 在更新模式下用到,根据主键更新目标表(单表时有效，多表时自动检测)

      STEP                    : 每次同步多少行
      LAST_EXE_TIME           : 最后执行时间(注意多表的情况)
      LAST_EXE_VALUE          : 最后同步的主键值(注意多表的情况)
      LAST_EXE_QTY            : 最后一次同步了多少行(注意多表的情况)
      EXE_QTY                 : 一共同步了多行行(注意多表的情况)
```